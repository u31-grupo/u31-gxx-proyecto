

import Menu from "../components/menu";


let anchoImg = "300px"
const ProductosPage = () => {

    return (
        <main>
            <h1>
                Lista de Productos
            </h1>
            <hr />
            
            <Menu />

            <hr />
            
            <br />
            <a href="./listaProductos">Ver Productos</a>

            <br /> <br />

            <img width={anchoImg} alt="Imagen No disponible" src="https://lms.uis.edu.co/mintic2022/pluginfile.php/1/theme_edumy/headerlogo1/1663168415/MisionTIC-UIS.png" />

            <br /> <br />

        </main>
    );
}


export default ProductosPage;