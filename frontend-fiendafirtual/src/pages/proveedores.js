import Menu from "../components/menu";


const ProveedoresPage = () => {
    return (
        <main>
            <h1>
                Lista de Proveedores
            </h1>
            <hr />

            <Menu />
            
            <hr />

            <br />
            <h2>
                Contenido adicional...
            </h2>
            <hr /><br />

        </main>
    );
}


export default ProveedoresPage;