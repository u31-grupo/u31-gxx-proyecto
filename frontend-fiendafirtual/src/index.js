import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
// import reportWebVitals from './reportWebVitals';
// Establecer las rutas o enlaces de la aplicación
// Previamente debo instalar => npm i react-router-dom
import {BrowserRouter as Router} from 'react-router-dom';

// import ProductosPage from './pages/productos';
// import ProveedoresPage from './pages/proveedores';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // <React.StrictMode>
    // <App variable = "Ejemplo variable paso por parámetros..." />
    <Router>
      <App />
    </Router>
  // </React.StrictMode>
);


// const productos = ReactDOM.createRoot(document.getElementById('productos'));
// productos.render(
//   <ProductosPage />
// );

// const proveedores = ReactDOM.createRoot(document.getElementById('proveedores'));
// proveedores.render(
//   <ProveedoresPage />
// );


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
